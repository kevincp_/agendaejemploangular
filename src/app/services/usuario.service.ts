import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Http, RequestOptions, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class UsuarioService {
  usuarios:any[];
  uriUsu = "http://localhost:3000/api/v1/usuario/";
  constructor(
    private http:Http,
    private router:Router
  ) {}

  getUsuario(){
    return this.http.get(this.uriUsu).map(res =>{
      this.usuarios = res.json();
    })
  }

  public autenticar(usuario:any) {
    let uriUsuario:string = "http://localhost:3000/auth/";
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    //headers.append('authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZFVzdWFyaW8iOjEsIm5pY2siOiJASkh1ZXJ0YXMiLCJjb250cmFzZW5hIjoiMTIzNCIsImlhdCI6MTQ5OTk2NzgwMCwiZXhwIjoxNDk5OTcxNDAwfQ.r_8l_8Is_oxopIQWamCLGm5iJL2UfXcOaimRkYO9bWM');

    let options = new RequestOptions({headers : headers});
    let data = JSON.stringify(usuario);

    this.http.post(uriUsuario, data, options)
    .subscribe(res => {
      console.log(res.json());
      let token = res.json().token;
      if(token) {
        console.log("Si existe el token");
        localStorage.setItem('token', token);
        //localStorage.setItem('idUsuario', res.json().USUARIO.idUsuario);
        this.router.navigate(['/dashboard/contacto']);
      } else {
        console.log("No existen token");
        return false;
      }
    }, error => {
      console.log(error.text());
    })

  }

  public verificarUsuario():boolean {
    if(localStorage.getItem('token')) {
      return true;
    }
    return false;
  }

  public registrar(usuario : any){
      let uriUsuario : string = "http://localhost:3000/api/v1/usuario";
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');

      let option = new RequestOptions({headers : headers});
      let data = JSON.stringify(usuario);

      return this.http.post(uriUsuario, data, option).map(res =>{
        return res.json();
      }, error =>{
        console.log(error.text());
      })
  }
  
} 
  
import { Component, OnInit } from '@angular/core';
import { TareaService } from '../../../services/tarea.service';

@Component({
  selector: 'app-tarea',
  templateUrl: './tarea.component.html',
  styleUrls: []
})
export class TareaComponent implements OnInit {
	tareas : any[];
  	constructor(private tareaService: TareaService) { }

  ngOnInit() {
  	this.inicializar();
  }

  borrarTarea(idTarea : any){
  	this.tareaService.eliminarTarea(idTarea).subscribe(data =>{
  		this.inicializar();
  	});
  }

  public inicializar(){
  	this.tareaService.getTarea().subscribe(data =>{
  		this.tareas = data;
  	});
  }

}

import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { TareaService } from '../../../services/tarea.service'; 


@Component({
  selector: 'app-tarea-form',
  templateUrl: './tarea-form.component.html',
  styleUrls: []
})
export class TareaFormComponent implements OnInit {
	formularioTarea : FormGroup;
	uri:string;
	tarea:any;
	notificacion:any = {
		estado:false,
		mensaje: ""
	}

  constructor(
  	private tareaService : TareaService,
  	private router: Router,
  	private activatedRoute: ActivatedRoute
  	) { 
  	  	let validacion = [
  		Validators.required, Validators.minLength(3)
  	];

	this.activatedRoute.params.subscribe(params => {
      this.uri = params["idTarea"];
      if(this.uri !== "nuevo") {
        this.tareaService.getTareas(params["idTarea"])
        .subscribe(tarea => {
          this.tarea = tarea;
          this.formularioTarea = new FormGroup({
            'titulo': new FormControl(this.tarea.titulo, validacion),
            'descripcion': new FormControl(this.tarea.descripcion, validacion),
            'fechaInicial': new FormControl(this.tarea.fechaInicial, validacion),
            'fechaFinal': new FormControl(this.tarea.fechaFinal, validacion),
            'estado': new FormControl(this.tarea.estado, validacion)
          });
        });
      } else {
        this.formularioTarea = new FormGroup({
          'titulo': new FormControl('', validacion),
          'descripcion': new FormControl('', validacion),
          'fechaInicial': new FormControl('', validacion),
          'fechaFinal': new FormControl('', validacion),
          'estado': new FormControl('', validacion)
        });
      }
    });
	}

  ngOnInit() {
  }

	public guardarCambios() {
	    if(this.uri === "nuevo") {
	      console.log("Nueva Tarea");
	      console.log(this.formularioTarea.value);
	      this.tareaService.nuevaTarea(this.formularioTarea.value)
	      .subscribe(res => {
	          this.notificacion.mensaje = res.mensaje;
	          this.notificacion.estado = res.estado;
	          setTimeout(() => {
	            this.router.navigate(['/dashboard/tarea']);
	          }, 3000);
	      });
	    } else {
	      console.log("Modificacion de contacto");
	      this.tareaService.editarTarea(this.formularioTarea.value, this.uri)
	      .subscribe(res => {
	        console.log(res);
	        setTimeout(()=>{
	          this.router.navigate(['/dashboard/tarea']);
	        }, 3000);
	      });
	    }
	}
}

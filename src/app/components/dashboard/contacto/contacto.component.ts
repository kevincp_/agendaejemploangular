import { Component, OnInit } from '@angular/core';
import { ContactoService } from '../../../services/contacto.service';

@Component({
  selector: 'app-contacto',
  templateUrl: './contacto.component.html',
  styles: []
})
export class ContactoComponent implements OnInit {
	contactos:any[];

  constructor(private contactoService: ContactoService) { }

  ngOnInit() {
  	this.inicializar();
  }

  borrarContacto(idContacto : any){
  	this.contactoService.eliminarContacto(idContacto).subscribe(data =>{
  		this.inicializar();
  	});
  }

  public inicializar(){
  	this.contactoService.getContacto().subscribe(data =>{
  		this.contactos = data;
  	})
  }
}

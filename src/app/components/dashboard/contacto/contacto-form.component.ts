import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ContactoService } from '../../../services/contacto.service'; 
import { CategoriaService} from '../../../services/categoria.service';

@Component({
  selector: 'app-contacto-form',
  templateUrl: './contacto-form.component.html',
  styleUrls: []
})
export class ContactoFormComponent implements OnInit {
	formularioContacto:FormGroup;
	uri:string;
	contacto:any;
	category: any[] = [];
	notificacion:any = {
		estado:false,
		mensaje: ""
	}

  constructor(
  	private categoriaService : CategoriaService,
  	private contactoService : ContactoService,
  	private router : Router,
  	private activatedRoute : ActivatedRoute
  	) {
  	let validacion = [
  		Validators.required, Validators.minLength(3)
  	];

  	this.activatedRoute.params.subscribe(params => {
      this.uri = params["idContacto"];
      if(this.uri !== "nuevo") {
        this.contactoService.getContactos(params["idContacto"])
        .subscribe(contacto => {
          this.contacto = contacto;
          this.formularioContacto = new FormGroup({
            'nombreContacto': new FormControl(this.contacto.nombreContacto, validacion),
            'apellido': new FormControl(this.contacto.apellido, validacion),
            'direccion': new FormControl(this.contacto.direccion, validacion),
            'telefono': new FormControl(this.contacto.telefono, validacion),
            'correo': new FormControl(this.contacto.correo, validacion),
            'idCategoria': new FormControl(this.contacto.idCategoria)
          });
        });
      } else {
        this.formularioContacto = new FormGroup({
          'nombreContacto': new FormControl('', validacion),
          'apellido': new FormControl('', validacion),
          'direccion': new FormControl('', validacion),
          'telefono': new FormControl('', validacion),
          'correo': new FormControl('', validacion),
          'idCategoria': new FormControl('')
        });
      }
    });
	}
  	

  ngOnInit() {
  	this.categoriaService.getCategoria().subscribe(data=>{
  		this.category = data;
  	});
  }

  public guardarCambios() {
    if(this.uri === "nuevo") {
      console.log("Nuevo Contacto");
      console.log(this.formularioContacto.value);
      this.contactoService.nuevoContacto(this.formularioContacto.value)
      .subscribe(res => {
          this.notificacion.mensaje = res.mensaje;
          this.notificacion.estado = res.estado;
          setTimeout(() => {
            this.router.navigate(['/dashboard/contacto']);
          }, 3000);
      });
    } else {
      console.log("Modificacion de contacto");
      this.contactoService.editarContacto(this.formularioContacto.value, this.uri)
      .subscribe(res => {
        console.log(res);
        setTimeout(()=>{
          this.router.navigate(['/dashboard/contacto']);
        }, 3000);
      });
    }
  }
}


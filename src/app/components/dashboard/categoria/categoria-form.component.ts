import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CategoriaService } from '../../../services/categoria.service'; 

@Component({
  selector: 'app-categoria-form',
  templateUrl: './categoria-form.component.html',
  styleUrls: []
})
export class CategoriaFormComponent implements OnInit {
	formularioCategoria : FormGroup;
	uri : string;
	categoria : any;
	notificacion : any = {
		estado: false,
		mensaje: "" 
	}

  constructor(
  	private categoriaService : CategoriaService,
  	private router : Router,
  	private activatedRoute : ActivatedRoute
  	){
  	let validacion = [
  		Validators.required, Validators.minLength(3)
  	];

  	 this.activatedRoute.params.subscribe(params => {
      this.uri = params["idCategoria"];
      if(this.uri !== "nuevo") {
        this.categoriaService.getCategorias(params["idCategoria"])
        .subscribe(categoria => {
          this.categoria = categoria;
          //console.log(this.categoria.nombreCategoria);
          this.formularioCategoria = new FormGroup({
            'nombreCategoria': new FormControl(this.categoria.nombreCategoria, validacion)
          });
        });
      } else {
        this.formularioCategoria = new FormGroup({
          'nombreCategoria': new FormControl('', validacion),

        });
      }
    });
}
  ngOnInit() {
  }

  public guardarCambios(){
  	if(this.uri === "nuevo"){
  		console.log("Nueva Categoria");
  		console.log(this.formularioCategoria.value);
  		this.categoriaService.nuevaCategoria(this.formularioCategoria.value).subscribe(res =>{
  				this.notificacion.mensaje = res.mensaje;
  				this.notificacion.estado = res.estado;
  			setTimeout(()=>{
  				this.router.navigate(['dashboard/categoria']);
  				}, 3000);	
  			
  		});
  	} else {
  		console.log("Modificacion de categorias");
  		this.categoriaService.editarCategoria(this.formularioCategoria.value, this.uri).subscribe(res =>{
  			this.formularioCategoria.reset();
        setTimeout(()=>{
          this.router.navigate(['dashboard/categoria']);
        }, 3000)
        console.log(res);
  		});
  	}
  }

}

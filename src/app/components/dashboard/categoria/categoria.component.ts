import { Component, OnInit } from '@angular/core';
import { CategoriaService } from '../../../services/categoria.service';


@Component({
  selector: 'app-categoria',
  templateUrl: './categoria.component.html',
  styles: []
})
export class CategoriaComponent implements OnInit {
    categorias:any[];
    categoriaCargada:any;
  	constructor(private categoriaService : CategoriaService) { }

  ngOnInit() {
  	this.inicializar();
  }
  
  borrarCategoria(idCategoria : any){
    this.categoriaService.eliminarCategoria(idCategoria).subscribe(res =>{
         this.inicializar();
    });
  }
  
  public inicializar(){
    this.categoriaService.getCategoria().subscribe(data =>{
      this.categorias = data;
    })
  }
}
